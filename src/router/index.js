import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/About',
    name: 'about us',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/Products',
    name: 'products',
    // redirect:'/ProductList',
    component: () => import(/* webpackChunkName: "products" */ '../views/Products.vue')
    // children:[
    //   {
    //     path: 'ProductList',
    //     name: 'ProductList',
    //     component: () => import(/* webpackChunkName: "productList" */ '../views/ProductList.vue')
    //   },
    //   {
    //     path: 'ProductDetail',
    //     name: 'ProductDetail',
    //     component: () => import(/* webpackChunkName: "productsdetail" */ '../views/ProductDetail.vue')
    //   },
    // ]
  },
  {
    path: '/ProductDetail',
    name: 'products',
    component: () => import('../views/ProductDetail.vue')
  },
  {
    path: '/Solution',
    name: 'solution',
    component: () => import('../views/Solution.vue')
  },
  {
    path: '/SolutionDetail',
    name: 'solution',
    component: () => import('../views/SolutionDetail.vue')
  },
  {
    path: '/News',
    name: 'news',
    component: () => import('../views/News.vue')
  },
  {
    path: '/NewDetail',
    name: 'news',
    component: () => import('../views/NewDetail.vue')
  },
  {
    path: '/Contact',
    name: 'contact us',
    component: () => import('../views/Contact.vue')
  },
  {
    path: '/Message',
    name: 'contact us',
    component: () => import('../views/Message.vue')
  },
  {
    path: '*',
    redirect: '/'
  }
]

const router = new VueRouter({
  routes,
  // mode: 'hash'
  // mode: 'history'
})

export default router
