import Vue from 'vue'
import Vuex from 'vuex'
import { getChildFunList, GetParentTypeList, getSingle } from '@/utils/api'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    cpMenu: [],
    vxContact: {
      proplist: {}
    }
  },
  mutations: {
    
    initCpMenu(state, data) {
      state.cpMenu = data
    },
    initContact(state, data){
      state.vxContact = data
    }

  },
  actions: {
    get_cpMenu(context, id) {
      getChildFunList({ pid:id }).then(res=>{
        let Menu = res.data.message
        for (let i = 0; i < Menu.length; i++) {
          GetParentTypeList({ funid:Menu[i].Id }).then(res=>{
            Menu[i]["subClass"] = res.data.message
          })
        }
        context.commit('initCpMenu', Menu)
      })
    },

    getContact(context, id) {
      getSingle({ funid:id }).then(res=>{
        context.commit('initContact', res.data.message)
      })
    }
  },
  getters: {


  },
  modules: {
    
  }
})
