import axios from 'axios'
// import baseUrl from '@/utils/baseURL'

const instance = axios.create({
  // baseURL: 'http://test47.xinshidian.top',
  baseURL: process.env.VUE_APP_TITLE == "生产环境" ? process.env.VUE_APP_BASE_API : '',
  // baseUrl,
  // 超时时间
  timeout: 5000,
});

// 添加请求拦截器
instance.interceptors.request.use(function (config) {
  // console.log(1);
  // 在发送请求之前做些什么 ==> 判断用户是否是登录状态
  return config;
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error);
});


// 添加响应拦截器
instance.interceptors.response.use(function (response) {
  // console.log(2);
  // 2xx 范围内的状态码都会触发该函数。
  // 对响应数据做点什么
  return response;
}, function (error) {
  // 超出 2xx 范围的状态码都会触发该函数。
  // 对响应错误做点什么
  return Promise.reject(error);
});

export default instance