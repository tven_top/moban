let baseURL = '';

if (process.env.VUE_APP_TITLE == '开发环境') { //注意变量名是自定义的
    baseUrl = "/api"  //开发环境url
}else if(process.env.VUE_APP_TITLE == '生产环境'){
    baseUrl = "http://test47.xinshidian.top"   //生产环境url
}

export default baseURL;