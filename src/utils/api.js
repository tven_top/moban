// export const getTopics = () => server({ url: '/v1/topics' })
import request from '@/utils/request';

// banner
// export function getBanner(obj){
//   return request({
//     url:'/api/api/GetOnePageInfoList',
//     params: obj
//   })
// }

// 查询菜单下属所有子类菜单
export function getChildFunList(params){
  return request({
    url:'/api/api/GetChildFunListByParentId',
    params
  })
}
// 查询菜单下属所有一级分类
export const GetParentTypeList = (params) => request({url:'/api/api/GetParentTypeListByFunId', params})

// 推荐接口
export const getHot = (params) => request({url:'/api/api/GetTopInfoList',params})

// 获取一条数据（根据菜单编号）
export const getSingle = (params) => request({url:'/api/api/GetSingleInfoByFunId', params})

// 获取一条数据（详情页）
export const getInfo = (params) => request({url:'/api/api/GetSingleInfoById', params})

// 获取一页列表数据
export const getOnePageList = (params) => request({url:'/api/api/GetOnePageInfoList', params})

// 查询总数
export const getCount = (params) => request({url:'/api/api/GetCount', params})

// SEO-link
export const getlinkSEO = (params) => request({url:'/api/pageseo/GetPageSeo_link', params})

// SEO-detail
export const getDetailSEO = (params) => request({url:'/api/pageseo/GetPageSeo_detail', params})

// 留言信息提交
// export const PostMess = (data, headers) => request({url:'/api/api/mess', method:'post', data, headers})


// export function PostMess(formData){
//   return request({
//     url: '/api/api/mess',
//     method: 'post',
//     data: formData,
//     headers: {
//       'Content-Type': 'multipart/form-data'
//     }
//   })
// }