import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './plugins/axios'

// ElementUI
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);

/* 引入swiper */
import VueAwesomeSwiper from 'vue-awesome-swiper/dist/vue-awesome-swiper'
// import 'swiper/css/swiper.css'
import 'swiper/css/swiper.min.css'
Vue.use(VueAwesomeSwiper);


/* vue-meta */
import VueMeta from 'vue-meta';
Vue.use(VueMeta);

// 引入 seo接口方法
import { getLinkSEO, getDetailSEO } from '@/utils/api'


Vue.config.productionTip = false

// 全局前置守卫
router.afterEach((to, form, next) => {
  if(to.path != form.path){
    window.scrollTo(0,0);
  }
})


// new Vue({  // 生成一个VUE实例
//   router,
//   store,
//   render: h => h(App),  // 渲染函数,将组件App渲染到挂载的元素上。
  
// }).$mount('#app')  //这个 #app 是挂载在 public 主页面上的，你打开index 就会看到 这个元素 id

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
  components: { App },
  template: '<App/>',
  data: {
    viewId: 1,
    metaInfo: {
      title: "",
      keywords: "",
      description: ""
    }
  },
  created() {
    // 全局配置
    router.beforeEach((to, from, next) => {
      let viewName = to.name;
      console.log(to.name)
      if(viewName == 'About'){
        viewName = 'About'
      }else if(viewName == 'Products' || viewName == 'ProductDetail'){
        viewName = 'Products'
      }else if(viewName == 'Solution' || viewName == 'SolutionDetail'){
        viewName = 'Solution'
      }else if(viewName == 'News' || viewName == 'NewDetail'){
        viewName = 'News'
      }else if(viewName == 'Contact' || viewName == 'Message'){
        viewName = 'Contact'
      }
      if(to.query.name) {
        this.getSEO(viewName, to.query.name);
      }else{
        this.getSEO2(viewName);
      }
      next();
    })
  },
  metaInfo() {
    return {
      title: this.metaInfo.title,
      meta: [
        {
          name: "keywords",
          content: this.metaInfo.keywords
        }, {
          name: "description",
          content: this.metaInfo.description
        }
      ]
    }
  },
  methods: {
    //获取seo
    
    getSEO(pname, id) {

      // getDetailSEO({pname:pname, id:id}).then(res=>{
      //   this.metaInfo.title = res.data.message.seotitle;
      //   this.metaInfo.keywords = res.data.message.seokey;
      //   this.metaInfo.description = res.data.message.seodesc;
      // })
      axios.get('/api/pageseo/GetPageSeo_detail/?pname='+pname+'&id='+id).then((response)=>{
        let res = response.data;
        if(res.status == 1){
          console.log(res.message);
          this.metaInfo.title = res.message.seotitle;
          this.metaInfo.keywords = res.message.seokey;
          this.metaInfo.description = res.message.seodesc;
        }else{
          this.$message('错误');
        }
      })
    },
    getSEO2(pname) {
      // getLinkSEO({pname}).then(res=>{
      //   this.metaInfo.title = res.data.message.seotitle;
      //   this.metaInfo.keywords = res.data.message.seokey;
      //   this.metaInfo.description = res.data.message.seodesc;
      //   console.log(this.metaInfo.description)
      // })
      axios.get('/api/pageseo/GetPageSeo_link/?pname='+pname).then((response)=>{
        let res = response.data;
        if(res.status == 1){
          // console.log(res.message);
          this.metaInfo.title = res.message.seotitle;
          this.metaInfo.keywords = res.message.seokey;
          this.metaInfo.description = res.message.seodesc;
        }else{
          this.$message('错误');
        }
      })
    }

  },
}).$mount('#app')


