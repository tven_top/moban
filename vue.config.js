module.exports={
	// publicPath: "/",
	devServer: {
			proxy: {
					"/api": {
						target: "http://test47.xinshidian.top", // 目标路径
						ws: true,        //如果要代理 websockets，配置这个参数
						secure: false,   // 如果是https接口，需要配置这个参数
						changeOrigin: true, //是否支持跨域
						pathRewrite:{  // 重写路径
							"^/api":""
						} 
					},
					// '/juyihui':{
					// 	target: 'http://101.43.34.10:2222', // 目标路径
					// 	changeOrigin: true, //是否支持跨域
					// 	pathRewrite:{
					// 		'^/juyihui':''
					// 	} // 重写路径
					// }
			}
	},
	//去掉空格报错
	lintOnSave: false
}